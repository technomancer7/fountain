#!/usr/local/bin/ruby
$LOAD_PATH.unshift "#{Dir.home}/.ft.d"
$LOAD_PATH.unshift "#{Dir.home}/.ft.d/extensions"
require 'opt'
require 'libkaiser'
require 'gtkp'
require "gtk2"
require "gtksourceview2"
require 'json'

=begin #test
  0 1 2 3 4 5 6 7 8
0 ==4=============
1 +---+----------+
2 | 1 |     2    |
3 |   |          |
4 |   |          |
5 |   |          |
6 |   |          |
7 +---+----------+
8 ==5=============

1. Tree View of files
2. SourceView showing raw chapter file contents (used just for the line markers and other nice features really, not the highlighting)
4. Menus
5. Status Bar

New format for files:
json files for every group

chapters handled as txt files
/story/chapters/1.fsf
/story/people/player.json
/story/places/home.json
=end
class FountainGUI
  def initialize
    @opt = OptHandler.new
    @dir = "#{Dir.home}/.ft.d/"
    @ext_dir = "#{Dir.home}/.ft.d/extensions/"
    @stories_dir = "#{Dir.home}/.ft.d/stories/"
    if not File.exist?(@dir); Dir.mkdir(@dir); end
    if not File.exist?(@ext_dir); Dir.mkdir(@ext_dir); end
    if not File.exist?(@stories_dir); Dir.mkdir(@stories_dir); end
    if not File.exist?("#{@dir}config.json")
      File.open("#{@dir}config.json", "w+") { |file| file.write("{}") }
      @config = {}
      @config["theme"] = {}
      @config["theme"]["main_txt_font"] = "Monospace 9"
      @config["internals"] = {}
      @config["internals"]["blacklist"] = []
      @config["internals"]["autoloads"] = []
      writeConfig
    else
      @config = JSON.parse(File.read("#{@dir}config.json"))
    end

    @entities_container = {}
    @chapters_container = {}
    @openchapter = ""
    @storypath = ""
    
    @extensions = {}
    # Root window
    @root_window = Gtk::Window.new
    @root_window.signal_connect('destroy') { Gtk.main_quit }
    @root_window.set_size_request(650, 500)
    @layout = Gtk::Table.new 8, 8, false
    @root_window.add @layout

    # Main story view
    @txts = Gtk::ScrolledWindow.new
    @cbox = Gtk::SourceView.new
    @txts.add @cbox
    @txts.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
    @cbox.show_line_marks = true
    @cbox.show_line_numbers = true
    @cbox.wrap_mode = Gtk::TextTag::WRAP_WORD
    @layout.attach @txts, 1, 6, 1, 6, Gtk::EXPAND|Gtk::FILL, Gtk::EXPAND|Gtk::FILL, 1, 1


    # Chapter/Entity browser
    @chb = Gtk::ScrolledWindow.new
    @chb.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)

    @chapters = Gtk::TreeView.new(treestore = Gtk::TreeStore.new(String))
    @chapters.append_column(Gtk::TreeViewColumn.new("Chapter", Gtk::CellRendererText.new,  :text => 0))
    @chb.add @chapters

    @layout.attach @chb, 0, 1, 1, 6, Gtk::FILL, Gtk::FILL, 1, 1
    
    @chapters.signal_connect("cursor-changed") do |v|
      if v.selection.selected != nil
        s = v.selection.selected[0]
        puts "Updating chapter selection... #{s}"
        @chapters_container[@openchapter] = @cbox.buffer.text
        @openchapter = s
        @cbox.buffer.text = ""
        @cbox.buffer.text = @chapters_container[s] if @chapters_container[s] != nil
      end

    end 
    
    column = @chapters.get_column(0)
    column.sort_indicator=true
    column.sort_column_id = 0
    column.signal_connect('clicked') do |w|
      w.sort_order =  w.sort_order == Gtk::SORT_ASCENDING ? Gtk::SORT_DESCENDING : Gtk::SORT_ASCENDING
    end    

    # Menu bar
    menubar = Gtk::MenuBar.new

    file = Gtk::MenuItem.new("File")
    edit = Gtk::MenuItem.new("Edit")
    ext = Gtk::MenuItem.new("Extend")
    help = Gtk::MenuItem.new("Help")
    filemenu = Gtk::Menu.new
    editmenu = Gtk::Menu.new
    extmenu = Gtk::Menu.new
    helpmenu = Gtk::Menu.new
    file.submenu = filemenu
    edit.submenu = editmenu
    ext.submenu = extmenu
    help.submenu = helpmenu
    menubar.append(file)
    menubar.append(edit)
    menubar.append(ext)
    menubar.append(help)
    
    group = Gtk::AccelGroup.new
    new = Gtk::ImageMenuItem.new(Gtk::Stock::NEW, group)
    open = Gtk::ImageMenuItem.new(Gtk::Stock::OPEN, group)
    save = Gtk::ImageMenuItem.new(Gtk::Stock::SAVE, group)
    
    
    new.signal_connect('activate') { |w|
      openPrompt("New Story", "Enter new story name: "){ |name| 
        createStoryDirectory name
        openStoryUI @stories_dir+name
      }
    }
    open.signal_connect('activate') { |w| promptOpenStoryUI }
    
    save.signal_connect('activate') { |w|
      saveStory
    }

    achp = Gtk::MenuItem.new("Add Chapter")
    achp.signal_connect('activate') { |w|
      openPrompt("New Chapter", "Enter chapter name: ") { |name| 
        addToTree @chapters, name
        File.open("#{@storypath}/chapters/#{name}.txt", "w+") { |file| file.write("") }
      }
    }

    dchp = Gtk::MenuItem.new("Delete Chapter")
    dchp.signal_connect('activate') { |w|
      ask("Delete #{@openchapter}", "Delete #{@openchapter}? \nThis can't be undone!") { |r|
        if r
          File.delete "#{@storypath}/chapters/#{@openchapter}.txt"
          @chapters_container = {}
          @openchapter = ""
          @chapters.model.clear
          @cbox.buffer.text = ""
          Dir.foreach("#{@storypath}/chapters/") do |obj|
            next if obj == ".."
            if obj.end_with? ".txt"
              addToTree @chapters, obj.split(".")[0]
              @chapters_container[obj.split(".")[0]] = File.read("#{@storypath}/chapters/#{obj}")
            end
          end
        end
      }
    }
        
    
    filemenu.append(new)
    filemenu.append(open)
    filemenu.append(save)
    filemenu.append achp
    filemenu.append dchp
    npad = Gtk::MenuItem.new("Notepad")
    npad.signal_connect('activate') { |w|
      openNotepad
    }
    editmenu.append npad
    
    setv = Gtk::MenuItem.new("Settings")
    setv.signal_connect('activate') { |w|
      f = HashEditor.new @config
      f.callback { |jso| 
        @config = jso
        writeConfig
      }
    }
    editmenu.append setv
    
    entv = Gtk::MenuItem.new("Open Entity Viewer")
    entv.signal_connect('activate') { |w| openEntities}
    editmenu.append entv
    
    fontv = Gtk::MenuItem.new("Set Storyview Font")
    fontv.signal_connect('activate') { |w| puts "TODO: Open font view" }
    editmenu.append fontv
    
    extb = Gtk::MenuItem.new("Extensions")
    extb.signal_connect('activate') { |w| openExtensionManager }
    extmenu.append extb

    extev = Gtk::MenuItem.new("Live Console")
    extev.signal_connect('activate') { |w| openEval }
    extmenu.append extev
    
    vbox = Gtk::VBox.new(homogeneous = false, spacing = nil)
    vbox.pack_start_defaults(menubar)
    #vbox.pack_start_defaults(label)
    @layout.attach vbox, 0, 6, 0, 1, Gtk::EXPAND|Gtk::FILL, Gtk::SHRINK

    
    # Finalize and run
    @root_window.show_all

    loadExtensions

    if @opt.command() != ""
      openStoryUI @stories_dir+@opt.command()
      if @opt.hasFlag? "e"
        openEntities
      end
    end
    Gtk.main
  end

  def openNotepad
    if @notepad == nil
      @notepad = Gtk::Window.new
      @notepad_container = Gtk::ScrolledWindow.new
      @notepad_text = Gtk::SourceView.new
      @notepad_text.show_line_marks = true
      @notepad_text.show_line_numbers = true
      @notepad_text.wrap_mode = Gtk::TextTag::WRAP_WORD
      @notepad_container.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS)
      @notepad.add @notepad_container
      @notepad_container.add @notepad_text
      @notepad.set_size_request(350, 400)
      @notepad.signal_connect('destroy') {
        File.open("#{@storypath}/notepad.txt", "w+") { |file| file.write(@notepad_text.buffer.text) }
        @notepad = nil
      }
      if File.exist?("#{@storypath}/notepad.txt")
        @notepad_text.buffer.text = File.read("#{@storypath}/notepad.txt")
      else
        @notepad_text.buffer.text = "This is a notepad area. Anything written here is stored and saved when the window closes."
      end
      
      @notepad.show_all
    end
  end
  
  def saveStory
    @chapters_container[@openchapter] = @cbox.buffer.text
    @chapters_container.each { |k, v|
      next if k == ""
      File.open("#{@storypath}/chapters/#{k}.txt", "w+") { |file| file.write(v) }
    }
    @entities_container.each { |grpname, grpv|
      grpv.each { |k, v|
        File.open("#{@storypath}/#{grpname}/#{k}.json", "w+") { |file| file.write(v.to_json) }
      }
    }
  end
  
  def openStoryUI(path)
    @root_window.title = path.split("/")[-1]
    @cbox.buffer.text = ""
    @entities_container = {}
    @chapters_container = {}
    @openchapter = ""
    @storypath = path
    @chapters.model.clear
    puts path
    Dir.foreach("#{path}/") do |entry|
      next if entry == ".."
      next if entry == "."
      next if not File.directory? path+"/"+entry
      Dir.foreach("#{path}/#{entry}/") do |obj|
        if entry == "chapters"
          if obj.end_with? ".txt"
            addToTree @chapters, obj.split(".")[0]
            @chapters_container[obj.split(".")[0]] = File.read("#{path}/#{entry}/#{obj}")
          end
        else
          @entities_container[entry] = {} if @entities_container[entry] == nil
          if obj.end_with? ".json"
            @entities_container[entry][obj.split(".")[0]] = JSON.parse(File.read("#{path}/#{entry}/#{obj}"))
          end
        end
      end
    end
  end

  def openEntities
    if @ent_win == nil          
      @ent_win = Gtk::Window.new
      @ent_win.signal_connect('destroy') { @ent_win = nil }
      @ent_win.set_size_request(370, 150)
      @ent_sc = Gtk::ScrolledWindow.new
      @ent_sc.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
      @ent_win.add @ent_sc
      openGroupsWindow
    end
  end

  def openEntityEdit(entity)
    @ent_grp = Gtk::VBox.new false
    @ent_grpe = {}
    @ent_grp_controls = Gtk::HBox.new
    backb = Gtk::Button.new "<<"
    backb.signal_connect("clicked"){
      @ent_grpe.each { |k, v|
        key = v[:key].text
        val = v[:val].text
        @entities_container[@eview_selected_group][@eview_selected_entity][key] = val
      }
      @ent_grp.destroy
      puts @entities_container
      openEntitiesWindow @entities_container[@eview_selected_group]
    }
    @ent_grp_controls.add backb
    crb = Gtk::Button.new "Create entry"
    crb.signal_connect("clicked"){
      openPrompt("New entry", "Enter entry for new #{@eview_selected_entity}:"){ |newname| 
        @entities_container[@eview_selected_group][@eview_selected_entity][newname] = ""
        @ent_grp.destroy
        openEntityEdit @entities_container[@eview_selected_group][@eview_selected_entity]
      }
    }
    @ent_grp_controls.add crb

    
    @ent_grp.add @ent_grp_controls
    entity.each { |k, v|
      @ent_grpe[k] = {}
      @ent_grpe[k][:box] = Gtk::HBox.new
      @ent_grpe[k][:key] = Gtk::Entry.new
      @ent_grpe[k][:val] = Gtk::Entry.new
      @ent_grpe[k][:key].text = k
      @ent_grpe[k][:val].text = v

      @ent_grpe[k][:box].add @ent_grpe[k][:key]
      @ent_grpe[k][:box].add @ent_grpe[k][:val]

      @ent_grpe[k][:del] = Gtk::Button.new "-"
      @ent_grpe[k][:del].signal_connect("clicked") { |w| 
        ask("Delete #{k}", "Delete #{k} from #{@eview_selected_group}.#{@eview_selected_entity}? This can't be undone."){ |r|
          if r
            puts "Deleting #{k}"
            @entities_container[@eview_selected_group][@eview_selected_entity].delete(k)
            @ent_grp.destroy
            openEntityEdit(@entities_container[@eview_selected_group][@eview_selected_entity])
          end
        }
      }
      @ent_grpe[k][:box].add @ent_grpe[k][:del]
      @ent_grp.add @ent_grpe[k][:box]
    }
    
    @ent_sc.add_with_viewport @ent_grp
    @ent_win.show_all
  end
  
  def openEntitiesWindow(grpf)
    @ent_els = Gtk::VBox.new
    @ent_hbs = {}
    @ent_edit_controls = Gtk::HBox.new
    backb = Gtk::Button.new "<<"
    backb.signal_connect("clicked"){
      @ent_els.destroy
      openGroupsWindow
    }
    @ent_edit_controls.add backb
    crb = Gtk::Button.new "Create #{@eview_selected_group}"
    crb.signal_connect("clicked"){
      openPrompt("New object", "Enter name for new #{@eview_selected_group}:"){ |newname| 
        @entities_container[@eview_selected_group][newname] = {}
        @ent_els.destroy
        openEntitiesWindow @entities_container[@eview_selected_group]
      }
    }
    @ent_edit_controls.add crb
    
    
    @ent_els.add @ent_edit_controls
    grpf.each { |grpname, grp|
      @ent_hbs[grpname] = {}
      @ent_hbs[grpname][:data] = grp
      @ent_hbs[grpname][:box] = Gtk::HBox.new
      @ent_hbs[grpname][:label] = Gtk::Label.new grpname
      @ent_hbs[grpname][:button] = Gtk::Button.new "Open"
      @ent_hbs[grpname][:label].ellipsize = Pango::EllipsizeMode::END
      @ent_hbs[grpname][:button].signal_connect("clicked") { |w|
        @eview_selected_entity = grpname.dup
        @ent_els.destroy
        puts "#{@eview_selected_group} >> #{@eview_selected_entity} >> #{grp}"
        openEntityEdit grp
      }
      
      @ent_hbs[grpname][:mv] = Gtk::Button.new "Move"
      @ent_hbs[grpname][:mv].signal_connect("clicked") { |w| 
        choices("Move #{grpname}", "Move #{grpname} from #{@eview_selected_group} to: ", @entities_container.keys){ |r|
          if r
            # Create new
            @entities_container[r][grpname] = @entities_container[@eview_selected_group][grpname].dup
            if File.exist? "#{@storypath}/#{@eview_selected_group}/#{grpname}.json"
              File.rename("#{@storypath}/#{@eview_selected_group}/#{grpname}.json",
                          "#{@storypath}/#{r}/#{grpname}.json")
            end

            
            @entities_container[@eview_selected_group].delete(grpname)
            @ent_els.destroy
            openEntitiesWindow(@entities_container[@eview_selected_group])
          end
        }
      }
      
      @ent_hbs[grpname][:del] = Gtk::Button.new "Delete"
      @ent_hbs[grpname][:del].signal_connect("clicked") { |w| 
        puts "Ask to delete #{grpname}, redraw window."
        ask("Delete #{grpname}", "Delete #{grpname} from #{@eview_selected_group}? This can't be undone."){ |r|
          if r
            @entities_container[@eview_selected_group].delete(grpname)
            File.delete("#{@storypath}/#{@eview_selected_group}/#{grpname}.json")
            @ent_els.destroy
            openEntitiesWindow(@entities_container[@eview_selected_group])
          end
        }
      }
      
      
      @ent_hbs[grpname][:box].add @ent_hbs[grpname][:button]
      @ent_hbs[grpname][:box].add @ent_hbs[grpname][:mv]
      @ent_hbs[grpname][:box].add @ent_hbs[grpname][:del]
      @ent_hbs[grpname][:box].add @ent_hbs[grpname][:label]
      @ent_els.add @ent_hbs[grpname][:box]
      
    }

    @ent_sc.add_with_viewport @ent_els
    @ent_win.show_all
  end
  
  def openGroupsWindow
    @ent_ls = Gtk::VBox.new
    @ent_hbs = {}
    addb = Gtk::Button.new "Create Group"
    addb.signal_connect("clicked"){
      openPrompt("New group", "Enter name for new group:"){ |newname| 
        @entities_container[newname] = {}
        Dir.mkdir("#{@storypath}/#{newname}/")
        @ent_ls.destroy
        openGroupsWindow
      }
    }
    @ent_ls.add addb
    @entities_container.each { |grpname, grp|
      @ent_hbs[grpname] = {}
      @ent_hbs[grpname][:data] = grp
      @ent_hbs[grpname][:box] = Gtk::HBox.new
      @ent_hbs[grpname][:label] = Gtk::Label.new grpname
      @ent_hbs[grpname][:label].ellipsize = Pango::EllipsizeMode::END
      @ent_hbs[grpname][:button] = Gtk::Button.new "Open"
      
      @ent_hbs[grpname][:button].signal_connect("clicked") { |w| 
        @eview_selected_group = grpname.dup

        @ent_ls.destroy
        openEntitiesWindow grp.dup
        
      }
      @ent_hbs[grpname][:del] = Gtk::Button.new "Delete"
      
      @ent_hbs[grpname][:del].signal_connect("clicked") { |w| 
        puts "Ask to delete #{grpname}, redraw window."
        ask("Delete #{grpname}", "Delete #{grpname}? This can't be undone."){ |r|
          if r
            @entities_container.delete(grpname)
            Dir.delete("#{@storypath}/#{grpname}/")
            @ent_ls.destroy
            openGroupsWindow
          end
        }
      }

      @ent_ls.add @ent_hbs[grpname][:box]
      
      @ent_hbs[grpname][:box].add @ent_hbs[grpname][:button]
      @ent_hbs[grpname][:box].add @ent_hbs[grpname][:del]
      @ent_hbs[grpname][:box].add @ent_hbs[grpname][:label]
    }

    @ent_sc.add_with_viewport @ent_ls
    @ent_win.show_all
  end

  
  def promptOpenStoryUI
    dialog = Gtk::FileChooserDialog.new("Open Story Directory", @root_window, Gtk::FileChooser::ACTION_SELECT_FOLDER, nil,
                                        [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
                                        [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
    dialog.current_folder = "#{@stories_dir}"
    if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT; openStoryUI dialog.filename;end;dialog.destroy;
  end

  def createStoryDirectory(name)
    if not File.exist?("#{Dir.home}/.ft.d/#{name}")
      Dir.mkdir("#{@stories_dir}#{name}/")
      Dir.mkdir("#{@stories_dir}#{name}/entities/")
      File.open("#{@stories_dir}#{name}/entities/protagonist.json", "w+") { |file| file.write("{}") }
      Dir.mkdir("#{@stories_dir}#{name}/chapters/")
      File.open("#{@stories_dir}#{name}/chapters/prologue.txt", "w+") { |file| file.write("") }
    else;puts "Story #{name} already exists."; end
  end
  
  def get(cat, name)
    en = []; Dir.foreach("#{@stories_dir}/#{name}/#{cat}/") do |entry| en.append(entry) if entry.end_with? ".json"; end; en;
  end
  
  def writeConfig
    File.open("#{Dir.home}/.ft.d/config.json", "w+") { |file|
      file.write(@config.to_json)
    }
  end
  
  def autoloadExtension(ext_id)
    @config["internals"]["autoloads"].append ext_id
    writeConfig
  end
  
  def unautoloadExtension(ext_id)
    @config["internals"]["autoloads"].delete ext_id
    writeConfig
  end
  
  def blacklistExtension(ext_id)
    @config["internals"]["blacklist"].append ext_id
    writeConfig
  end
  
  def unblacklistExtension(ext_id)
    @config["internals"]["blacklist"].delete ext_id
    writeConfig
  end
  
  def setv(grp, key, value)
    unless @config[grp].key? key
      @config[grp] = {}
    end
    @config[grp][key] = value
    writeConfig
  end
  
  def getv(grp, key, default)
    @config[grp] = {} unless @config.key? grp
    if @config[grp].key? key
      return @config[grp][key]
    end
    default
  end
  
  def openExtensionManager
    @extwin = Gtk::Window.new
    @extwin.set_size_request(350, 150)
    @extlay = Gtk::ScrolledWindow.new
    @extlay.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS)
    @hb = Gtk::VBox.new
    @extlay.add_with_viewport @hb
    
    Dir.foreach("#{Dir.home}/.ft.d/extensions/") do |entry|
      if entry.end_with? ".rb"
        e = Gtk::CheckButton.new(entry.split(".")[0])
        if @extensions[entry.split(".")[0]][:enabled]
          e.active = true
        end
        e.signal_connect( "toggled" ) { |w|
          if w.active? #turn off ext
            send "load_#{w.label}", self
            @extensions[entry.split(".")[0]][:enabled] = true
          else
            send "unload_#{w.label}", self
            @extensions[entry.split(".")[0]][:enabled] = false
          end
        }
        @hb.add e
      end
    end
    

    @extwin.add @extlay
    @extwin.show_all
  end

  def loadExtensions
    blacklist = []
    autos = []
    blacklist = @config["internals"]["blacklist"] unless @config["internals"]["blacklist"] == nil
    autos = @config["internals"]["autoloads"] unless @config["internals"]["autoloads"] == nil
    Dir.foreach("#{Dir.home}/.ft.d/extensions/") do |entry|
      if entry.end_with? ".rb"
        puts "Loading extension #{entry} in to memory..."

        unless blacklist.include? entry.split(".")[0]
          @extensions[entry.split(".")[0]] = {ui_elements: {}, enabled: false}
          require entry.split(".")[0]

          if autos.include? entry.split(".")[0]
            send "load_#{entry.split(".")[0]}", self
            @extensions[entry.split(".")[0]][:enabled] = true
          end
        end
      end
    end
  end

  def openEval
    if @codewin != nil and @coutwin != nil
      @codewin.destroy
      @coutwin.destroy
    else
      @codewin = Gtk::Window.new
      @codes = Gtk::ScrolledWindow.new
      ruby_lang = Gtk::SourceLanguageManager.new.get_language('ruby')
      revalbuf = Gtk::SourceBuffer.new ruby_lang
      @cbox = Gtk::SourceView.new revalbuf
      @cbox.show_line_marks = true
      @cbox.show_line_numbers = true
      @cbox.wrap_mode = Gtk::TextTag::WRAP_WORD
      @codes.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
      @coutwin = Gtk::Window.new
      @couts = Gtk::ScrolledWindow.new
      @cout = Gtk::TextView.new
      @cout.wrap_mode = Gtk::TextTag::WRAP_WORD
      @couts.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
      @codewin.title = "Live Evaluation"
      @codewin.set_size_request(500, 450)
      @coutwin.set_size_request(450, 200)
      @codewin.signal_connect('destroy') { @codewin = nil }
      @code = Gtk::Table.new 5, 6, false
      @cout.buffer.create_tag("err",           {"underline"     => Pango::Underline::SINGLE, "foreground" => "#ff0000"})
      @coutwin.add @couts
      @couts.add @cout
      @coutwin.title = "Eval Output"
      @coutwin.show_all
      @codewin.add @codes
      @codes.add @cbox
      @codewin.show_all
      
      @cbox.signal_connect('key_press_event') { |w, e|
        if e.keyval == 65474
          begin
            result = eval @cbox.buffer.text
            @cout.buffer.text += "#{result}\n"
          rescue Exception => e  
            @cout.buffer.text += e.message+"\n"
            tag @cout, "#{e.message}", "err"
            for bt in e.backtrace
              @cout.buffer.text += "-- #{bt}\n"
            end
          end
          @cout.buffer.text += "---------------\n"
        end
      }
    end 
  end
end


if __FILE__ == $0
  FountainGUI.new
end
