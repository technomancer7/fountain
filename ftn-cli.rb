#!/usr/local/bin/ruby
$LOAD_PATH.unshift "#{Dir.home}/.ft.d"
$LOAD_PATH.unshift "#{Dir.home}/.ft.d/extensions"
require 'opt'
require 'libkaiser'
require 'json'

VERSION = "0.0.1"
FTN = "#{Dir.home}/.ft.d/"
STORIES = "#{Dir.home}/.ft.d/stories/"

$opt = OptHandler.new
$story = $opt.get("story")
$story = $opt.get("s") if $story == nil

def validateStory(subd = nil)
  if $story == nil; puts "Required story name: --story:<name>"; exit 1; end
  dir = "#{STORIES}#{$story.to_s}/"
  if not File.exist?(dir); Dir.mkdir(dir); end
  if not File.exist?(dir+"chapters"); Dir.mkdir(dir+"chapters"); end
  if subd != nil; if not File.exist?(dir+subd); Dir.mkdir(dir+subd); end; end
  dir
end

def createStoryDirectory(name)
  if not File.exist?("#{STORIES}#{name}")
    Dir.mkdir("#{STORIES}#{name}/")
    Dir.mkdir("#{STORIES}#{name}/entities/")
    File.open("#{STORIES}#{name}/entities/protagonist.json", "w+") { |file| file.write("{}") }
    Dir.mkdir("#{STORIES}#{name}/chapters/")
    File.open("#{STORIES}#{name}/chapters/prologue.txt", "w+") { |file| file.write("") }
    puts "New chapter: [#{$opt.command(2).blue}]"
  else;puts "Story #{name} already exists."; end
end

def main
  case $opt.command(0, "help")
  when "help"
    puts "FTN-CLI #{VERSION}"
    puts "Commands:"
    puts "new"
    puts "write"
    puts "delete"
    puts "ls"
    puts "set"
    puts "get"

  when "set"
    gk = $opt.command(1)
    unless gk.include? "."
      puts "Format: set group[.entity].key value"
    end
    g = gk.split(".")[0]
    k = gk.split(".")[1..-1].join(".")
    v = $opt.command(2)
    puts "#{g.blue}.#{k.blue} = #{v.blue}"
    if g == "global"
      config = {}
      config = JSON.parse(File.read("#{FTN}config.json")) if File.exist?(FTN+"config.json");
      config[k] = v
      File.open("#{FTN}config.json", "w+") { |file| file.write(config.to_json) }
      return 0
    end
    
    dir = validateStory
    if g == "config"
      config = {}
      config = JSON.parse(File.read("#{dir}story.json")) if File.exist?(dir+"story.json");
      config[k] = v
      File.open("#{dir}story.json", "w+") { |file| file.write(config.to_json) }
    elsif g != "chapters"
      unless k.include? "."
        puts "Bad format";return 1
      end
      e = k.split(".")[0]
      k = k.split(".")[1]
      unless Dir.exist? "#{dir}#{g}/"
        puts "Group #{g} doesn't exist. Creating..."
        Dir.mkdir "#{dir}#{g}/"
      end
      config = {}
      config = JSON.parse(File.read("#{dir}#{g}/#{e}.json")) if File.exist?("#{dir}#{g}/#{e}.json");
      config[k] = v
      File.open("#{dir}#{g}/#{e}.json", "w+") { |file| file.write(config.to_json) }
    else
      puts "Invalid set group."
      return 1
    end
    
  when "write", "w"
    editor = $opt.get("editor", "nano")
    dir = validateStory
    c = $opt.command(1, 'prologue')
    puts "Running #{editor} \"#{dir}chapters/#{c}.txt\""
    system "#{editor} \"#{dir}chapters/#{c}.txt\""
    
  when "delete", "d", "remove", "r"
    if $opt.command(1) != ""
      dir = "#{STORIES}#{$opt.command(1)}"
      if Dir.exist? dir
        print "Really delete #{dir}/*? (CAN NOT BE REVERSED) `y` to confirm > "
        if gets.chomp == "y"
          `rm -r #{dir}`
        end
      end
    end
    
  when "new"
    if $opt.get("h") or $opt.get("help")
      puts "new [entity/group/chapter/story]"
      puts "new entity <group> <name>"
      puts "new chapter <name>"
      puts "new group <name>"
      
      return
    end
    
    type = $opt.command(1)
    case type
    when "entity", "e"
      if $opt.command(2) != "" and $opt.command(3) != ""
        if $opt.command(2) == "chapters"; puts "Use [new chapter <name] to create chapters.";return;end
        dir = validateStory $opt.command(2)
        File.open("#{dir}/#{$opt.command(2)}/#{$opt.command(3)}.json", "w+") { |file| file.write("{}") }
        puts "New [#{$opt.command(2).blue}] created: #{$opt.command(3).blue}"
      end
    when "group", "g"
      dir = validateStory
      if $opt.command(2) != ""
        if not File.exist?(dir+$opt.command(2))
          Dir.mkdir(dir+$opt.command(2))
          puts "New group: [#{$opt.command(2).blue}]"
        end
      end
    when "chapter", "c"
      dir = validateStory
      if not File.exist?(dir+$opt.command(2)+".txt")
        File.open(dir+$opt.command(2)+".txt", "w+") { |file| file.write("") }
        puts "New chapter: [#{$opt.command(2).blue}]"
      end
    when "story", "s"
      createStoryDirectory $opt.command(2) if $opt.command(2) != ""
    else
      if $opt.command(1) != "" and $opt.command(2) != ""
        if $opt.command(1) == "chapters"; puts "Use [new chapter <name] to create chapters.";return;end
        dir = validateStory $opt.command(1)
        File.open("#{dir}#{$opt.command(1)}/#{$opt.command(2)}.json", "w+") { |file| file.write("{}") }
        puts "New [#{$opt.command(1).blue}] created: #{$opt.command(2).blue}"
      end
    end
    
  end

  0
end


exitcode = main
exit exitcode
